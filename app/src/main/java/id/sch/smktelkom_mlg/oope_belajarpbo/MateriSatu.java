package id.sch.smktelkom_mlg.oope_belajarpbo;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

import id.sch.smktelkom_mlg.oope_belajarpbo.adapter.MateriSatuAdapter;
import id.sch.smktelkom_mlg.oope_belajarpbo.model.Satu;

public class MateriSatu extends AppCompatActivity {
    ArrayList<Satu> mList = new ArrayList<>();
    MateriSatuAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_materi_satu);

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new MateriSatuAdapter(mList);
        recyclerView.setAdapter(mAdapter);
        fillData();
    }

    private void fillData() {
        Resources resources = getResources();
        String[] arJudul = resources.getStringArray(R.array.satu_materi);
        String[] arDeskripsi = resources.getStringArray(R.array.satu_materi_desc);
        TypedArray a = resources.obtainTypedArray(R.array.satu_materi_picture);
        Drawable[] arFoto = new Drawable[a.length()];
        for (int i = 0; i < arFoto.length; i++) {
            arFoto[i] = a.getDrawable(i);

        }
        for (int i = 0; i < arJudul.length; i++) {
            mList.add(new Satu(arJudul[i], arDeskripsi[i], arFoto[i]));
        }
        mAdapter.notifyDataSetChanged();
    }

}

package id.sch.smktelkom_mlg.oope_belajarpbo.model;

import android.graphics.drawable.Drawable;

/**
 * Created by Dna on 08/05/2018.
 */

public class Satu {
    public String judul, deskripsi;
    public Drawable foto;

    public Satu(String judul, String deskripsi, Drawable foto) {
        this.judul = judul;
        this.deskripsi = deskripsi;
        this.foto = foto;
    }
}

package id.sch.smktelkom_mlg.oope_belajarpbo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayout;
import android.view.View;
import android.widget.Toast;

public class GridActivity extends AppCompatActivity {

    GridLayout mainGrid;
    CardView cardview1, cardview2, cardview3, cardview4, cardview5, cardview6, cardview7, cardview8, cardview9;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid);

        mainGrid = findViewById(R.id.mainGrid);
        cardview1 = findViewById(R.id.cvSatu);
        cardview2 = findViewById(R.id.cvDua);
        cardview3 = findViewById(R.id.cvTiga);
        cardview4 = findViewById(R.id.cvEmpat);
        cardview5 = findViewById(R.id.cvLima);
        cardview6 = findViewById(R.id.cvEnam);
        cardview7 = findViewById(R.id.cvTujuh);
        cardview8 = findViewById(R.id.cvDelapan);
        cardview9 = findViewById(R.id.cvSembilan);

        //set event
        setSingleEvent(mainGrid);

        cardview1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(GridActivity.this, Kuis1Activity.class);
                startActivity(i);
                finish();
            }
        });
    }

    private void setSingleEvent(GridLayout mainGrid) {
        //loop all child item of main grid
        for (int i = 0; i < mainGrid.getChildCount(); i++) {
            CardView cardView = (CardView) mainGrid.getChildAt(i);
            final int finalI = i;
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(GridActivity.this, "Anda memilih item nomor " + finalI, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}

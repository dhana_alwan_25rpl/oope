package id.sch.smktelkom_mlg.oope_belajarpbo;

/**
 * Created by Dna on 02/05/2018.
 */

public class Data {
    private String name;
    private String desc;
    private int id;

    public Data(String name, String desc, int id) {
        this.name = name;
        this.desc = desc;
        this.id = id;
    }
}

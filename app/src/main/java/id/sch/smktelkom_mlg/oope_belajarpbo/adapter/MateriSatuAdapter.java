package id.sch.smktelkom_mlg.oope_belajarpbo.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import id.sch.smktelkom_mlg.oope_belajarpbo.R;
import id.sch.smktelkom_mlg.oope_belajarpbo.model.Satu;

/**
 * Created by Dna on 08/05/2018.
 */

public class MateriSatuAdapter extends
        RecyclerView.Adapter<MateriSatuAdapter.ViewHolder> {
    ArrayList<Satu> satuList;

    public MateriSatuAdapter(ArrayList<Satu> satuList) {
        this.satuList = satuList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_satu, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Satu satu = satuList.get(position);
        holder.tvJudul.setText(satu.judul);
        holder.tvDeskripsi.setText(satu.deskripsi);
        holder.ivFoto.setImageDrawable(satu.foto);

    }

    @Override
    public int getItemCount() {
        if (satuList != null)
            return satuList.size();
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvJudul, tvDeskripsi;
        ImageView ivFoto;

        public ViewHolder(View itemView) {
            super(itemView);
            ivFoto = itemView.findViewById(R.id.imageView);
            tvJudul = itemView.findViewById(R.id.textViewJudul);
            tvDeskripsi = itemView.findViewById(R.id.textViewDeskripsi);
        }
    }
}
